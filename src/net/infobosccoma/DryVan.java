/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

/**
 *
 * @author kessy
 */
public class DryVan extends Contenidor {

    String color;

    public DryVan() {
    }

    public DryVan(String color, String numSerie, int capacitat, boolean estat, Mercaderia[] mercaderia) {
        super(numSerie, capacitat, estat, mercaderia);
        setColor(color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if (color == null || color.equals("")) {
            throw new IllegalArgumentException("Color incorrecte");
        }
    }

}
