/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

/**
 *
 * @author kessy
 */
public class Cisterna extends Contenidor {

    final static int LITRES = 1000;

    public Cisterna() {
    }

    public Cisterna(String numSerie, int capacitat, boolean estat, Mercaderia[] mercaderia) {
        super(numSerie, capacitat, estat, mercaderia);
    }

    @Override
    public int getCapacitat() {
        return capacitat;
    }

    @Override
    public void setCapacitat(int capacitat) {
        this.capacitat = capacitat / LITRES;
    }
    

}
