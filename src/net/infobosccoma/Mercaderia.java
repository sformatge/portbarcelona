/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

/**
 *
 * @author kessy
 */
public class Mercaderia {

    private String descripcio, numSerie;
    private int Volum;

    public Mercaderia(String descripcio, int Volum, Contenidor contenidor) {
        setDescripcio(descripcio);
        setVolum(Volum);
        setNumSerie(contenidor);
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getVolum() {
        return Volum;
    }

    public void setVolum(int Volum) {
        this.Volum = Volum;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(Contenidor contenidor) {
        this.numSerie = contenidor.numSerie;
    }
    
    
    
}
