/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author kessy
 */
public class Vaixell {

    final static int MAX_CONTENIDOR = 1000;
    Contenidor contenidors[];

    public Vaixell() {
    }

    public Vaixell(Contenidor[] plataforma) {
        setContenidors(plataforma);
    }

    public Contenidor[] getPlataforma() {
        return contenidors;
    }

    public void setContenidors(Contenidor[] plataforma) {
        if (contenidors.length > MAX_CONTENIDOR) {
            throw new IllegalArgumentException();
        }

    }

    public void carregarContenidor(Contenidor contenidor) {
        if (contenidors.length >= MAX_CONTENIDOR && contenidor.estat == true) {
            contenidors[contenidors.length] = contenidor;
        }
    }

    public void descarregarVaixell() {
        int volum = 0;
        System.out.println("PORT DE BARCELONA");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String data = dateFormat.format(date);
        System.out.printf("DATA: ", data);
        System.out.println("CONTENIDOR          VOLUM");
        System.out.println("-------------------------");
        for (Contenidor contenidors : this.contenidors){
            String id = contenidors.getNumSerie();
            int capacitat = contenidors.getCapacitat();
            volum += capacitat;
            System.out.printf("%S           %d m3\n", id, capacitat);
        }
        System.out.printf("Volum total          %d", volum);
        this.contenidors = null;
    }

    public void mostrarContenidors() {
          
    }

}
