/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

/**
 *
 * @author kessy
 */
public class Refrigerat extends Contenidor{
    int temp_min;

    public Refrigerat(int temp_min, String numSerie, int capacitat, boolean estat, Mercaderia[] mercaderia) {
        super(numSerie, capacitat, estat, mercaderia);
        setTemp_min(temp_min);
    }

    public int getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(int temp_min) {
        this.temp_min = temp_min;
    }
    @Override
    public Mercaderia[] getMercaderies() {
        return mercaderies;
    }
    @Override
    public void setMercaderies(Mercaderia[] mercaderies) {
        for (Mercaderia mercaderia : mercaderies) {
            mercaderia.setVolum((int)Math.round(mercaderia.getVolum()+(mercaderia.getVolum()*(0.01/100))));
        }
        this.mercaderies = mercaderies;
    }
    
}
