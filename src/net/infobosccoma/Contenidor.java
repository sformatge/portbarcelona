/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma;

/**
 *
 * @author kessy
 */
public class Contenidor {

    String numSerie;
    int capacitat;
    boolean estat;
    Mercaderia mercaderies[];

    public Contenidor(String numSerie, int capacitat, boolean estat, Mercaderia[] mercaderies) {
        setNumSerie(numSerie);
        setCapacitat(capacitat);
        setEstat(estat);
        setMercaderies(mercaderies);
    }

    public Contenidor() {
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        if (numSerie == null || numSerie.equals("")) {
            throw new IllegalArgumentException("Número de Sèrie Invàlid");
        }
    }

    public int getCapacitat() {
        return capacitat;
    }

    public void setCapacitat(int cap) {
        capacitat = cap;
    }

    public boolean isEstat() {
        return estat;
    }

    public void setEstat(boolean estat) {
        this.estat = estat;
    }

    public Mercaderia[] getMercaderies() {
        return mercaderies;
    }

    public void setMercaderies(Mercaderia[] merca) {
        this.mercaderies = merca;
    }

    public void afegirMercaderies(Mercaderia[] mercaderia) {
        if (mercaderia.length < this.capacitat && estat == true) {
        }
    }
}
